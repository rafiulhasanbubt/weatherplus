//
//  TempFormat.swift
//  WeatherPlus
//
//  Created by Md. Kamrul Hasan on 9/7/19.
//  Copyright © 2019 rafiulhasan. All rights reserved.
//

import Foundation

struct TempFormat {
    static let celsius = "°C"
    static let fahrenheit = "°F"
}
