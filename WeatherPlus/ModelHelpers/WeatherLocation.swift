//
//  WeatherLocation.swift
//  WeatherPlus
//
//  Created by Md. Kamrul Hasan on 9/7/19.
//  Copyright © 2019 rafiulhasan. All rights reserved.
//

import Foundation

struct WeatherLocation: Codable, Equatable {
    var city: String!
    var country: String!
    var countryCode: String!
    var isCurrentLocation: Bool!
}
