//
//  APIURLS.swift
//  WeatherPlus
//
//  Created by Md. Kamrul Hasan on 9/7/19.
//  Copyright © 2019 rafiulhasan. All rights reserved.
//

import Foundation

let CURRENTLOCATION_URL = "https://api.weatherbit.io/v2.0/current?&lat=\(LocationService.shared.latitude!)&lon=\(LocationService.shared.logitude!)&key=b7b34b5e758549b3914b6e5f4fd77457"
let CURRENTLOCATIONWEEKLYFORECAST_URL = "https://api.weatherbit.io/v2.0/forecast/daily?lat=\(LocationService.shared.latitude!)&lon=\(LocationService.shared.logitude!)&days=7&key=b7b34b5e758549b3914b6e5f4fd77457"
let CURRENTLOCATIONHOURLYFORECAST_URL = "https://api.weatherbit.io/v2.0/forecast/hourly?lat=\(LocationService.shared.latitude!)&lon=\(LocationService.shared.logitude!)&hours=24&key=b7b34b5e758549b3914b6e5f4fd77457"
