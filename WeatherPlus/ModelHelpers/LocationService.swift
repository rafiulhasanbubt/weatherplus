//
//  LocationService.swift
//  WeatherPlus
//
//  Created by Md. Kamrul Hasan on 9/7/19.
//  Copyright © 2019 rafiulhasan. All rights reserved.
//

import Foundation

class LocationService {    
    static var shared = LocationService()
    
    var logitude: Double!
    var latitude:Double!
}
