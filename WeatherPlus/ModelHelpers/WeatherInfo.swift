//
//  WeatherInfo.swift
//  WeatherPlus
//
//  Created by Md. Kamrul Hasan on 8/7/19.
//  Copyright © 2019 rafiulhasan. All rights reserved.
//

import Foundation
import UIKit

struct WeatherInfo {
    let infoText: String!
    let nameText: String?
    let image: UIImage?
}
